#!/usr/bin/env python3
import sys
import pathlib
import bigfile
import mmap

def find_bin_in(bindata, path):
    if path.is_dir():
        print("Descending: ", path, file=sys.stderr)
        for child in path.iterdir():
            for res in find_bin_in(bindata, child): yield res
    elif path.is_file():
        print("Searching in: ", path, file=sys.stderr)
        with open(path, "rb") as f:
            mem = mmap.mmap(f.fileno(), 0, prot=mmap.PROT_READ)
            pos = mem.find(bindata)
            if pos != -1: yield (path, pos)

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: {} <filename> <find hash of filename in dir/file>"
            .format(sys.argv[0]), file=sys.stderr)
        sys.exit(1)
    filename = sys.argv[1]
    find_in = sys.argv[2]

    # AFAIK, DXHR filenames never have forward slashes in them
    # (at least in the Win version)
    # Make sure you don't have the language designation before the
    # proper path!
    filename = filename.replace("/", "\\")
    fhash = bigfile.Bigfile.get_filename_hash(filename)
    # For some reason, gibbed's tools output these hashes in big endian hex,
    # but they're saved in the (PC) files in LE
    fhash_bin = fhash.to_bytes(4, "little")
    print("Searching for hash", fhash_bin.hex(), file=sys.stderr)
    for res in find_bin_in(fhash_bin, pathlib.Path(find_in)):
        print(res)
