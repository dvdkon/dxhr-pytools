#!/usr/bin/env python3
import sys
import pathlib
import bigfile
import mmap

def find_bin_in(bindata, path):
    if path.is_dir():
        print("Descending: ", path, file=sys.stderr)
        for child in path.iterdir():
            for res in find_bin_in(bindata, child): yield res
    elif path.is_file():
        print("Searching in: ", path, file=sys.stderr)
        with open(str(path), "rb") as f:
            mem = mmap.mmap(f.fileno(), 0, prot=mmap.PROT_READ)
            pos = mem.find(bindata)
            if pos != -1: yield (path, pos)

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: {} <hex sequence> <search in dir/file>"
            .format(sys.argv[0]), file=sys.stderr)
        sys.exit(1)
    binseq_str = sys.argv[1]
    find_in = sys.argv[2]

    binseq = bytes.fromhex(binseq_str)
    print("Searching for bytes", binseq.hex(), file=sys.stderr)
    for res in find_bin_in(binseq, pathlib.Path(find_in)):
        print(res)
