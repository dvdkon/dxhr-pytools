#!/usr/bin/env python3
import sys
import os
import argparse
import struct
from enum import Enum
from ctypes import c_uint32
from collections import namedtuple
from binutils import BinUtils
from multifile import MultiFile

class Bigfile:
	"""
	Represents a set of "BIGFILE.[0-9]{3}" files typically found in the root
	directory of the game.
	"""
	Entry = namedtuple("Entry",
		("name_hash", "name", "size", "offset", "locale", "uncompressed_size"))

	class FileTypes(Enum):
		"""
		Enum of all filetypes of files contained in Bigfiles
		Values are file extensions
		"""
		bin = "bin" #Basically "uncompressed_size"
		#uncompressed and compressed variants of a container format
		drm = "drm"
		cdrm = "cdrm"
		usm = "usm" # cinematics
		png = "png"
		sam = "sam" #Sound metadata?
		mus = "mus" #Sound metadata
		mul = "mul" #Sound data
		raw = "raw"

		@classmethod
		def detect(cls, sample, endianness):
			"""sample - bytes object of len() 4 from the start of the file"""
			sample_as_uint = struct.unpack(
				("<" if endianness == "little" else ">") + "I", sample)[0]
			#sample_as_uint is a format version in this case?
			if sample_as_uint == 14 or sample_as_uint == 19 or\
				sample_as_uint == 21 or sample_as_uint == 22:
				return cls.drm
			elif sample == b"CDRM":
				return cls.cdrm
			elif sample == b"CRID":
				return cls.usm
			elif sample == b"\x89PNG":
				return cls.png
			elif sample == b"FSB4":
				return cls.sam
			elif sample[:3] == b"Mus":
				return cls.mus
			#sample_as_uint is bitrate in this case
			elif sample_as_uint == 22050 or sample_as_uint == 3200 or\
				sample_as_uint == 3600 or sample_as_uint == 44100 or\
				sample_as_uint == 4800 or\
				(sample_as_uint >= 43900 and sample_as_uint <= 44300) or\
				(sample_as_uint >= 31000 and sample_as_uint <= 32200):
				return cls.mul
			else:
				return cls.bin

	@staticmethod
	def get_filename_hash(filename):
		"""
		Uses the game's specific hashing method on a string.
		This kind of hash is being used as a filename substitute
		"""
		h = 0xffffffff
		for char in filename:
			h ^= ord(char) << 24
			for i in range(8):
				if h & 0x80000000 == 0:
					h <<= 1
				else:
					h = (h << 1) ^ 0x04c11db7

		return c_uint32(~h).value

	def __init__(self, f, filenames_file = None):
		"""
		f - input file
		filenames_file - file containing all the filenames of the files
			contained in this bigfile. This is used because of hashes being
			stored instead of actual filenames.
			Please close after calling.
		"""
		self.f = f
		self.bu = BinUtils(f)
		self.hashes = dict()
		if filenames_file != None:
			for line in filenames_file:
				#[:-1] - remove newlines
				self.hashes[Bigfile.get_filename_hash(line[:-1])] = line[:-1]

		self.entries = self.read_header()

	def get_filename(self, entry):
		if entry.name_hash in self.hashes:
			return self.hashes[entry.name_hash]
		else:
			self.f.seek(entry.offset)
			ft = Bigfile.FileTypes.detect(self.f.read(4), self.bu.endianness)
			return "_no_filename/" + hex(entry.name_hash)[2:] + "." + ft.value


	def read_header(self):
		file_alignment = self.bu.read_uint32()
		assert file_alignment == 0x7ff00000
		self.base_path = self.bu.f.read(64).decode("ASCII")
		entry_count = self.bu.read_uint32()
		entry_name_hashes = []
		for i in range(entry_count):
			entry_name_hashes.append(self.bu.read_uint32())
		entries = []
		for i in range(entry_count):
			pos = self.f.tell()
			entry = Bigfile.Entry(
				name_hash=entry_name_hashes[i],
				name=None,
				size=self.bu.read_uint32(),
				offset=self.bu.read_uint32(),
				locale=self.bu.read_uint32(),
				uncompressed_size=self.bu.read_uint32()
			)
			entry = entry._replace(name=self.get_filename(entry))
			entries.append(entry)
			self.f.seek(pos + 4 * 4) #four entries of uint32

		return entries

	def get_entry_by_hash_and_locale(self, hash_num, locale_num):
		entries = list(filter(
			lambda x: x.name_hash == hash_num and x.locale == locale_num,
			self.entries))
		entries_len = len(entries)
		assert entries_len > 0, "No entries found"
		assert entries_len <= 1, "More than one entry with a given hash"
		return entries[0]

	def extract_entry(self, entry):
		self.f.seek(entry.offset)
		return self.f.read(entry.size)

if __name__ == "__main__":
	parser = argparse.ArgumentParser(
		description="Read and write BIGFILE.000-999 files"
	)

	sps_mode = parser.add_subparsers(dest="mode")

	pp_input = argparse.ArgumentParser(add_help=False)
	pp_input.add_argument("-f", "--files", metavar="files", required=True,
		help="Files to extract", nargs="+")
	pp_input.add_argument("-n", "--filenames-file", metavar="filenames-file",
		help="File from which to load names for entries",
		default=os.path.dirname(sys.argv[0]) + "/filenames.txt")

	sp_extract = sps_mode.add_parser("extract", parents=[pp_input],
		help="Extract one file from the bigfiles")
	sp_extract.add_argument("-o", "--out", metavar="outfile", required=True,
		help="Output file")
	sp_extract.add_argument("-H", "--hash", metavar="hash", required=True,
		help="Hash of file to extract")
	sp_extract.add_argument("-l", "--locale", metavar="locale",
		help="Locale of file to extract", default="ffffffff")

	sp_extract_all = sps_mode.add_parser("extract-all", parents=[pp_input]
		)
	sp_extract_all.add_argument("-o", "--out", metavar="outdir", required=True,
		help="Output directory")

	sp_list = sps_mode.add_parser("list", parents=[pp_input])

	args = parser.parse_args()
	if args.mode in ("extract", "extract-all", "list"):
		infile = MultiFile([open(p, "rb") for p in args.files], binary=True)
		with open(args.filenames_file) as filenames_file:
			bigfile = Bigfile(infile, filenames_file=filenames_file)
			filenames_file.close()
		if args.mode == "list":
			for entry in bigfile.entries:
				print(hex(entry.name_hash)[2:], entry.name, entry.size, entry.offset,
					hex(entry.locale)[2:], entry.uncompressed_size)
		elif args.mode == "extract":
			if args.hash is None:
				sys.stderr.write(
					"Hash has to be specified with extract mode\n")
				sys.exit(1)
			entry = bigfile.get_entry_by_hash_and_locale(
				int(args.hash, 16), int(args.locale, 16))
			with open(args.out, "wb") as outfile:
				outfile.write(bigfile.extract_entry(entry))
		elif args.mode == "extract-all":
			pass

		infile.close()

