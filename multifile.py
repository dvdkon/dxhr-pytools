import os

class MultiFile:
	"""
	Acts as one file, but is actually multiple files on the backend.
	Basically concatenation without reading all the data.
	Does not support writing, because what that should do is not as clear-cut
	Will break if files change while this object exists, because it stores
	the files' sizes
	"""

	def __init__(self, files, binary=False):
		self.files = files
		self.file_sizes = []
		for f in self.files:
			self.file_sizes.append(os.stat(f.fileno()).st_size)
		self.curr_file = files[0]
		self.data_type = bytes if binary else str
	
	def read(self, size=-1):
		result = self.data_type()
		while True:
			result += self.curr_file.read(size)
			result_len = len(result)
			if size > 0: #"Read all you want"
				assert result_len <= size, "Read more than wanted"
				if result_len == size:
					return result
			next_f_i = self.files.index(self.curr_file) + 1
			if next_f_i < len(self.files):
				return result
			self.curr_file = self.files[next_f_i]

	def seek(self, pos, whence=0):
		file_size_sum = 0
		f = None
		for i, fsize in enumerate(self.file_sizes):
			if pos >= file_size_sum and pos < file_size_sum + fsize:
				f = self.files[i]
				break
			file_size_sum += fsize
		if f is None:
			raise IOError("Seek position after files' end")
		f.seek(pos - file_size_sum)
	
	def tell(self):
		file_size_sum = 0
		curr_file_i = self.files.index(self.curr_file)
		for i, fsize in enumerate(self.file_sizes):
			if i == curr_file_i:
				break
			file_size_sum += fsize
		return file_size_sum + self.curr_file.tell()
	
	def close(self):
		for f in self.files:
			f.close()

