#!/usr/bin/env python3
import sys
import bigfile

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: {} <filename>"
            .format(sys.argv[0]), file=sys.stderr)
        sys.exit(1)

    # AFAIK, DXHR filenames never have forward slashes in them
    # (at least in the Win version)
    # Make sure you don't have the language designation before the
    # proper path!
    # EDIT: Some things do have forward slashes in them, let's keep
    # this as non-confusing as possible
    #filename = filename.replace("/", "\\")
    filename = sys.argv[1]
    fhash = bigfile.Bigfile.get_filename_hash(filename)
    # For some reason, gibbed's tools output these hashes in big endian hex,
    # but they're saved in the (PC) files in LE
    fhash_bin = fhash.to_bytes(4, "little")
    print(fhash_bin.hex())
