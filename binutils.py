import struct

class BinUtils:
	"""
	Helper for reading binary files, which makes it easier to deal with
	endianness and makes code more readable by not explicitly stating data
	length/signedness.
	"""

	def __init__(self, src_file, endianness="little"):
		self.f = src_file
		self.endianness = endianness

	def read_and_unpack(self, fmt, data_len):
		return struct.unpack(
			("<" if self.endianness == "little" else ">") + fmt,
			self.f.read(data_len)
		)

	def read_uint32(self):
		return self.read_and_unpack("I", 4)[0]


