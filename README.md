# PyTools for Deus Ex: Human Revolution
This is a collection of scripts for exploring Deus Ex: Human Revolution's
datafiles.

`bigfile.py` lists all "subfiles" in the game files and optionally extracts
them. To do this it needs a file with all subfiles' names, one per line, since
filenames are stored in a hashed form. The most complete list can be
downloaded from
[a XeNTaX thread](http://forum.xentax.com/viewtopic.php?f=10&t=7208&start=510).

`look_for_hash.py` hashes a filename and looks for its binary representation
in a directory recursively. This can be useful for finding references to
files (or not, I'm not quite sure this'll lead anywhere).
